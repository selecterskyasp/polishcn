﻿<!--#include file="top.asp"-->
<!--#include file="conn.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="UTF-8" />
<meta name="author" content="toothbrushpolishcn">
<meta name="Copyright" content="Copyright (c) <%=year(now())%> www.biclean.com" />
<meta name="robots" content="all" />
<meta name="Keywords" content="About BICLEAN BRUSH CO.,LTD"/>
<meta name="Description" content="BICLEAN BRUSH CO.,LTD is one of the largest & leading toothbrush manufacturers in Yiwu, China,established 1997. We specialize in the development , production, and sale for toothbrushes and electric toothbrushes. Our good quality products with unique design will create comfortable feeling and effectively keep your oral fresh ever! Our machines equipped with Germany automatic machines and Korea auto packaging machines increase our capacity greatly about 1，100,000 pcs/day which make our factory can promise best price and delivery to our customers. Our products marked 'POLISH' won good reputation all over the world eapecially in East Asia,West Europe, both North and South America" />
<title>关于碧洁 | 碧洁刷业有限公司</title>
<!--#include file="esptop.asp"-->
	<table width="460" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="images/product1_03.gif" width="460" height="10" /></td>
      </tr>
      <tr>
        <td height="23" background="images/product_bg.gif" class="border_lr_blue" style="padding-left:8px">关于我们 &nbsp;&nbsp;&nbsp;<img src="images/arrow.gif" width="9" height="5" align="baseline" /></td>
      </tr>
      <tr>
        <td height="530" valign="top"  align="left" class="border_lrb_blue" style="padding:15px;text-align:justify;text-justify:inter-ideograph"><img src="images/about.jpg" width="172" height="178" align="left" style="padding-right:10px;" />
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <p > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;碧洁刷业有限公司始建于1997年，是集研发、设计、生产、销售为一体的专业化口腔护理用品制造型企业。经过这几年的开拓发展，公司现成长为现代化的中型牙刷生产企业。公司现有本科、大专以上的管理人员50多人，拥有从德国、韩国、台湾等地引进的具有世界先进水平的牙刷生产设备，年生产能力上亿支。产品远销欧洲、南美洲、非洲、亚洲在内的世界各地市场。公司自有主打品牌“新感觉”牌牙刷，在全国各地拥有一定数量的客户群和消费群体。公司凭借精良的技术设备、优质的原材料、先进的工艺配方、严密的科学管理，本着“品质重于一切”的公司理念，制造出优质的产品<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;我公司在2006年通过了享誉全球的瑞士通用公正行集团（SGS）ISO9001：2000国际质量管理体系认证，并于2009年5月份通过了（SGS）ISO9001：2008国际质量管理体系认证。2007年7月拿到了采用国际标准产品标志证书。辛勤的付出终于迎来了收获，我们的产品受到了国内外客户的青睐，这为提升产品国际市场竞争力和塑造国际品牌奠定了坚实的基础。 根据使用对象的不同，采用国际技术标准，开发，研制和生产各种规格的牙刷，产品有6大系列230个品种之多，拥有多项牙刷产品专利，具备年产上亿支牙刷的生产能力。根据当今社会发展和消费的需要，本公司已开发了电动系列牙刷,超声波电动牙刷，紫外线消毒牙刷盒，在牙刷史上翻开了新的一页.</p>
          <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;我们一直倡导“经营生活，关注健康”的人文关怀，推荐积极创新的工作理念。我们始终认为：您的支持是我们成长的动力，您的认可是我们壮大的基石！碧洁刷业全体员工诚挚感谢你的积极配合与热情参与，真诚地期盼与你携手，在新的起点上，一同创造美好的未来!</p></td>
      </tr>
    </table>
		<table width="95%" border="0" cellpadding="0" height="5">
		<tr>
		<td></td>
		</tr>		
		</table>
	</td>
  </tr>
</table>
<!--#include file="espfooter.asp"-->