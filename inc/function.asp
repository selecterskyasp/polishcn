﻿<%
function GetExtendName(FileName)
	dim ExtName
	ExtName = LCase(FileName)
	ExtName = right(ExtName,3)
	ExtName = right(ExtName,3-Instr(ExtName,"."))
	GetExtendName = ExtName
end function
Function getHTTPPage(Path)
        t = GetBody(Path)
        getHTTPPage=BytesToBstr(t,"utf-8")
End function

Function GetBody(url) 
        on error resume next
        Set Retrieval = CreateObject("Microsoft.XMLHTTP") 
        With Retrieval 
        .Open "Get", url, False, "", "" 
        .Send 
        GetBody = .ResponseBody
        End With 
        Set Retrieval = Nothing 
End Function

Function BytesToBstr(body,Cset)
        dim objstream
        set objstream = Server.CreateObject("adodb.stream")
        objstream.Type = 1
        objstream.Mode =3
        objstream.Open
        objstream.Write body
        objstream.Position = 0
        objstream.Type = 2
        objstream.Charset = Cset
        BytesToBstr = objstream.ReadText 
        objstream.Close
        set objstream = nothing
End Function

Function Newstring(wstr,strng)
        Newstring=Instr(lcase(wstr),lcase(strng))
        if Newstring<=0 then Newstring=Len(wstr)
End Function

'采用二进制数据流操作,为了解决编码问题生成静态页面
Sub SaveToFile(ByVal strBody,ByVal File)
    Dim objStream
    On Error Resume Next
    Set objStream = Server.CreateObject("ADODB"+"."+"Stream")
    If Err.Number=-2147221005 Then 
        Response.Write "<div align='center'>非常遗憾,您的主机不支持ADODB.Stream,不能使用本程序</div>"
        Err.Clear
        Response.End
    End If
    With objStream
        .Type = 2
        .mode = 3
        .Charset = "utf-8"
		.Open
        .Position = objStream.Size
        .WriteText = trim(strBody)
        .SaveToFile Server.MapPath(File),2
		.flush
        .Close
    End With
    Set objStream = Nothing
End Sub

function ReadFromUTF (TempString,charset)    'TempString要读取的模板文件路径; Charset是编码
    dim str
    set stm=server.CreateObject("adodb.stream")
    stm.Type=2 
    stm.mode=3 
    stm.charset=charset
    stm.open
    stm.loadfromfile server.MapPath(TempString)
    str=stm.readtext
    stm.Close
    set stm=nothing
    ReadFromUTF=str
	
end function 

'在指定的位置创建文件夹
function Creatfol(folpath,folname)
	dim fso
	set fso=server.CreateObject("Scripting.FileSystemObject")
	folpath=server.MapPath(folpath)&"/"&folname
	if not fso.FolderExists(folpath) then		
		fso.CreateFolder(folpath)
	end if	
	if err.number=0 then
		Creatfol=fso.GetBaseName(folpath)
	else
		Creatfol=false
	end if
	set fso=nothing
end function
'格式化价格
function FormatNum(num,n)
if num<1 then
num="0"&cstr(FormatNumber(num,n))
else
num=cstr(FormatNumber(num,n))
end if
FormatNum=replace(num,",","")
end function
'删除指定的文件夹
function Delfol(folpath,folname)
	dim fsobj
	set fsobj=server.CreateObject("Scripting.FileSystemObject")
	folpath=server.MapPath(folpath)&"/"&folname
	if not fsobj.FolderExists(folpath) then
		Delfol=true
		exit function		
	end if
	fsobj.DeleteFolder(folpath)
	if err.number=0 then
		Delfol=true
	else
		Delfol=false
	end if
	set fsobj=nothing
end function

'删除指定的文件
function DelFile(filename)
on error resume next
	dim delobj
	set delobj=server.CreateObject("Scripting.FileSystemObject")
	filename=server.MapPath(filename)	
	if delobj.FileExists(filename) then
		delobj.DeleteFile(filename)
	else
	delFile=false
	set delobj=nothing
	exit function
	end if
	set delobj=nothing
	if err.number=0 then
		DelFile=true
	else
		DelFile=false
	end if
end function

sub insert_record(table,Parameters,values)'表名,条件,返回路径
'	response.Write("insert into "&table&"("&Parameters&")values("&values&")")
'response.End()
	sql="insert into "&table&"("&Parameters&")values("&values&")"
	conn.execute(sql)
end sub

sub del_record(table,Conditions)'表名,条件,返回路径
'	response.Write("delete from "&table&" where "&Conditions&"")
'response.End()
	conn.execute("delete from "&table&" where "&Conditions&"")
end sub

sub update_record(table,Parameters,Conditions)'表名,条件,返回路径
	'response.Write("update "&table&" set "&Parameters&" where "&Conditions&"")
'response.End()
	conn.execute("update "&table&" set "&Parameters&" where "&Conditions&"")
end sub
Function select_recordset(fieldname,tablename,v_fieldname,v_fieldname_value)

	  sql_view="select "&fieldname&" from "&tablename&" where  "&v_fieldname&"="&v_fieldname_value&""
	  set rs_view=server.CreateObject("adodb.recordset")
	  rs_view.open sql_view,conn
	     if not rs_view.eof then
	        select_recordset=rs_view(fieldname)
	     end if 
	  rs_view.close
	  set rs_view=nothing
 end Function
'关闭对象
sub closers(Para)
	para.close
	set para=nothing
end sub
'释放资源
sub closeconn()
	conn.close
	set conn=nothing
end sub
function showmsg(msg)
	response.Write("<script>alert('您当前操作结果:\n\n"&msg&"');history.back();</script>")
	response.End()
end function
'-----根据ID取得name的Sub-----------------
Function getValueByID(sortID,inArray)
    dim i
    if NOT IsArray(inArray) then
        getValueByID=""
        Exit Function
    end if
    for i=0 to UBound(inArray,2)
        if Cstr(sortID)=Cstr(inArray(0,i)) then
            getValueByID=inArray(1,i) '返回name
            Exit Function
        end if
    next
    getValueByID=""
End Function
'判断指定文件夹是否为空
function isNullFolder(path)
 	dim fs,f,f1,fc,s,folderPath
	folderPath=server.mapPath(path)
	set fs=createobject("scripting.filesystemobject") 	
	if fs.FolderExists(folderPath) then
	set f=fs.GetFolder(folderPath)
	if f.size=0 then
	isNullFolder=true
	else
	isNullFolder=false
	end if
	set f=nothing
	else
	isNullFolder=false
	end if 
	set fs=nothing
end function

'********************************************
'函数名：IsValidEmail
'作  用：检查Email地址合法性
'参  数：email ----要检查的Email地址
'返回值：True  ----Email地址合法
'       False ----Email地址不合法
'********************************************
function IsValidEmail(email)
	dim names, name, i, c
	IsValidEmail = true
	names = Split(email, "@")
	if UBound(names) <> 1 then
	   IsValidEmail = false
	   exit function
	end if
	for each name in names
		if Len(name) <= 0 then
			IsValidEmail = false
    		exit function
		end if
		for i = 1 to Len(name)
		    c = Lcase(Mid(name, i, 1))
			if InStr("abcdefghijklmnopqrstuvwxyz_-.", c) <= 0 and not IsNumeric(c) then
		       IsValidEmail = false
		       exit function
		     end if
	   next
	   if Left(name, 1) = "." or Right(name, 1) = "." then
    	  IsValidEmail = false
	      exit function
	   end if
	next
	if InStr(names(1), ".") <= 0 then
		IsValidEmail = false
	   exit function
	end if
	i = Len(names(1)) - InStrRev(names(1), ".")
	if i <> 2 and i <> 3 then
	   IsValidEmail = false
	   exit function
	end if
	if InStr(email, "..") > 0 then
	   IsValidEmail = false
	end if
end function

'********************************************
'函数名：makePassword
'作  用：产生指定长度的随机数字
'参  数：maxLen ----随机数的长度
'返回值：产生的随机数
'********************************************
function   makePassword(byVal   maxLen)   
  Dim   strNewPass   
  Dim   whatsNext,   upper,   lower,   intCounter   
  Randomize   
    
  For   intCounter   =   1   To   maxLen   
  whatsNext   =   Int((1   -   0   +   1)   *   Rnd   +   0) 
  upper   =   57   
  lower   =   48   
  strNewPass   =   strNewPass   &   Chr(Int((upper   -   lower   +   1)   *   Rnd   +   lower))   
  Next   
  makePassword   =   strNewPass   
    
end   function
 
'********************************************
'函数名：makeNumber
'作  用：产生指定16位的定单号,同年月日和8位随机数字组成
'********************************************
function makeNumber()
dim t_y,t_m,t_d
t_y=cstr(year(now))
t_m=cstr(month(now))
t_d=cstr(day(now))
if len(t_m)<2 then t_m="0"&t_m
if len(t_d)<2 then t_d="0"&t_d
makeNumber=t_y&t_m&t_d&makePassword(8)
end function

'********************************************
'子程序名：showInfo
'作  用：显示浮动信息提示
'参  数：title:浮动窗口的标题;page:返回的页面;content:结果内容
'page为空时隐藏浮动窗口
'返回值：无
'********************************************
sub showInfo(title,page,content)
if page="" then
page="javascript:history.back()"
end if
Response.Write(_
	"<div id=""massage_box2"" style="" visibility:visible"">" & _
	"<div class=""massage"">" & _
	"<div class=""masktitle"">" & _
	"<div style=""display:inline; position:absolute; background:url(/images/login_key.gif) no-repeat; text-indent:20px;text-align:left;font-weight:bold; font-size:14px;text-align:left;"">"&title&"</div>" & _
	"<span onClick=""massage_box2.style.visibility='hidden'; mask2.style.visibility='hidden'"" style=""float:right; display:inline; cursor:hand; font-weight:bold; font-size:14px;"">×</span>" & _
	"</div>" & _
	"<div style=""padding:10px"">" & _
	"<div style=""padding:2px 0"">"&content&"</div>" & _
	"<div style=""padding:2px 0""><a href="""&page&""">返回</a></div>" & _
	"</div>" & _
	"</div>" & _
	"</div>" & _
	"<div id=""mask2"" style=""visibility:visible""></div>" & _
"")
Response.Write(_
	"<script language=""javascript"">" & _
	"function hiddenInfo()" & _
	"{" & _
	"massage_box2.style.visibility='hidden';" & _
	"mask2.style.visibility='hidden';" & _
	"}" & _
	"</script>" & _
"")
end sub

'================================================= 
  '函数名：JmailSend 
  '作 用：用Jmail发送邮件 
  '参 数：MailTo收件人地址 Subject邮件标题 Body内容 
  '返回值：flase 发送失败　true 发送成
function JmailSend(MailTo,Subject,Body) 
   dim JmailMsg,From,FromName,Smtp,Username,Password
  ' Body 邮件内容    
  ' MailTo 收件人Email 
  ' From 发件人Email 
  ' FromName 发件人姓名 
  ' Smtp smtp服务器 
  ' Username 邮箱用户名 
  ' Password 邮箱密码 
   From="root@xsp2.com"
   FromName="麦巴巴"
   Smtp="mail@xsp2.com"
   Username="root@xsp2.com"
   Password="wujinchen"
   
   set JmailMsg=server.createobject("jmail.message") 
   JmailMsg.charset="uft-8" 
   JmailMsg.ContentType = "text/html" 
   JmailMsg.ISOEncodeHeaders = False '是否进行ISO编码，默认为True
   JmailMsg.AddHeader "Originating-IP", Request.ServerVariables("REMOTE_ADDR")
   JmailMsg.from=From
   JmailMsg.AddRecipient MailTo 
   JmailMsg.MailDomain=Smtp   
   
   JmailMsg.MailServerUserName =Username
   JmailMsg.MailServerPassWord =Password  
   
   
   JmailMsg.fromname=FromName  

   
   JmailMsg.subject=Subject 
   JmailMsg.body=Body    
   
   JmailSend=JmailMsg.send("mail.xsp2.com") 
   
   JmailMsg.close 
   set JmailMsg=nothing    
end function 

'判断是否已经登录
function isLogin()
dim user,pass,userid
user=replace(trim(request.Cookies("islogin")("email")),"'","")
pass=replace(trim(request.Cookies("islogin")("pass")),"'","")
userid=request.Cookies("islogin")("id")

if len(user)<4 or len(pass)<>32 or chkrequest(userid)=false then 
	isLogin=false
	exit function
end if

set checkRs=server.CreateObject("adodb.recordset")
checkRs.open "select useremail from [user] where useremail='"&user&"' and password='"&pass&"' and id="&clng(userid)&"",conn,1,1
if checkRs.eof and checkRs.bof then
	call closers(checkrs)
	isLogin=false
	exit function
end if
call closers(checkrs)
isLogin=true
end function
%>