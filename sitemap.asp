<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="UTF-8" />
<meta name="author" content="toothbrushpolishcn">
<meta name="Copyright" content="Copyright (c) 2008 www.biclean.com" />
<meta name="robots" content="all" />
<meta name="Keywords" content=""/>
<meta name="Description" content="" />
<title>sitemap-POLISH</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="855" border="0" align="center" cellpadding="0" cellspacing="0" class="borer_l_r">
      <tr>
        <td width="24%"><img src="images/index_02.gif" width="207" height="93" /></td>
        <td width="76%" align="right" valign="top"><font color="#FFFFFF">English | <a href="espsitemap.asp"  class="white_link">中文版</a> | <a href="Sitemap.asp"  class="white_link">Sitemap</a></font>&nbsp;&nbsp;&nbsp;</td>
      </tr>
</table>
<table width="855" border="0" align="center" cellpadding="0" cellspacing="0" class="borer_l_r">
  <tr>
    <td height="24" background="images/index_04.gif"><font color="#067FCE">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="./" class="menu">Home</a> &nbsp; &nbsp; &nbsp;| &nbsp; &nbsp;&nbsp; <a href="abouttoothbrush.asp" class="menu"> About us</a>  &nbsp; &nbsp;&nbsp;| &nbsp; &nbsp;&nbsp; <a href="toothbrush.asp" class="menu">Products</a>  &nbsp; &nbsp;&nbsp;| &nbsp; &nbsp;&nbsp; <a href="Contactbiclean.asp" class="menu"> Contact us </a></font></td>
  </tr>
</table>
<table width="855" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="borer_l_r">
<tr><td colspan="2" height="22">&nbsp;</td></tr>
<tr>
    <td width="100" align="center" height="22"></td><td align="left"  style="border: 1px solid #5190CA;"><a href="index.asp" class="mapblue">English version</a></td>
	</tr>
	<tr><td colspan="2" height="22">&nbsp;</td></tr>
	<tr>
    <td width="100" align="center" height="22"></td><td align="left"  style="border: 1px solid #5190CA;"><a href="default.asp" class="mapblue">中文版 version</a></td>
	</tr>

	<tr><td colspan="2" height="22">&nbsp;</td></tr>
  <tr>
    <td width="100" align="center" height="22"></td><td align="left"  style="border: 1px solid #5190CA;"><a href="index.asp" class="mapblue">Home page</a></td>
	</tr>
	<tr><td colspan="2" height="22">&nbsp;</td></tr>
	 <tr>
     <td width="100" align="center" height="22"></td><td align="left"  style="border: 1px solid #5190CA;"><a href="abouttoothbrush.asp" class="mapblue">About Biclean</a></td>
	</tr>
	<tr><td colspan="2" height="22">&nbsp;</td></tr>
	 <tr>
    <td width="100" align="center" height="22"></td><td align="left"  style="border: 1px solid #5190CA;"><a href="toothbrush.asp" class="mapblue">Toothbrush Products</a></td>
	</tr>
	<tr><td colspan="2" height="22">&nbsp;</td></tr>
	 <tr>
    <td width="100" align="center" height="30"></td><td align="left"  style="border: 1px solid #5190CA;"><a href="toothbrushnewsdetail.asp?id=142" class="mapblue">Toothbrush News</a></td>
	</tr>
	<tr><td colspan="2" height="22">&nbsp;</td></tr>
	 <tr>
    <td width="100" align="center" height="30"></td><td align="left"  style="border: 1px solid #5190CA;"><a href="Contactbiclean.asp" class="mapblue">Contact Biclean</a></td>
	</tr>
	<tr><td colspan="2" height="22">&nbsp;</td></tr>
</table>

<table width="855" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="40" align="center" bgcolor="#F0EFF2">&copy; 2008 Biclean Company. All rights reserved. <span style="font-size: 18px;
	font-weight: bold;
	font-style: italic;
	color: #666666;">Biclean</span></td>
  </tr>
</table>
</body>
</html>